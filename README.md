# Architecture Ref. Card 03

Die Ref. Card 03 ist eine Spring Boot Application, welches die Benutzer erlaubt sich Witze anzuschauen. Die Witze werden aus einer Datenbank (Maria DB) abgerufen und auf der Website angezeigt.

## Voraussetzungen

   - Docker installiert
   - Maven installiert
   - Java installiert
   - Ein Gitlab Konto besitzen
   - Die folgende AWS Leistungen zu verfügung haben
        - ECS
        - ECR
        - RDS

## Inbetriebnahme und Verwendung

Projekt herunterladen

```sh
git clone https://gitlab.com/bbwrl/m346-ref-card-03.git
cd m346-ref-card-03
```

Projekt erstelln 

```sh
docker build -t <name>
```

Projekt starten

```sh
docker run -p <port>:8080 -e DB_URL="jdbc:mariadb://<DB-URL>" -e DB_USERNAME="<DB-benutzername>" -e DB_PASSWORD="<DB-passwort>" <name>
```

## AWS 

1. AWS Learner Lab starten 

2. RDS erstelln
    - Auf "Create database" klicken
    - Beim Engine type : "MariaDB" auswählen
    - Ein "Master password" erstellen und den Passwort merken
    - Beim Connectivity-Public access "Yes" auswählen
    - "Create database"

3. Ein Repository auf AWS ECR erstellen

4. Auf AWS Details klicken

5. Dockerfile pushen mit den tag latest

6. Eine ECS Task definition erstellen
    - Auf "Create new task definition" klicken
    - "Task definition family name" eingeben
    - Ein "Container name" und "Image URI" mit den tag latest eingeben
    - "Environment variables" erstellen
        - KEY: "DB_URL" VALUE: "jdbc:mariadb://DB-URL"
        - KEY: "DB_USERNAME" VALUE: "DB-benutzername"
        - KEY: "DB_PASSWORD" VLAUE: "DB-passwort"
    - den port 8080 mit TCP öffnen
    - "next" -> "next" -> "create" klicken

7. Eine ECS Cluster erstellen
    - Auf "Create" klicken
    - "Cluster name" eingeben
    - Auf "Create" klicken

8. Eine ECS Service erstellen
    - In neu erstellter Cluster auf "Create" klicken
    - Compute options "Launch type" auswählen
    - Beim Family die neu erstellte task definition auswählen
    - Auf "Create" klicken

9. Folgende CI/CD Varaiblen, mit Infos von AWS Details und AWS ECR, ergänzen unter Gitlab-Settings-CI/CD-Variables
    - AWS_DEFAULT_REGION
    - AWS_ACCESS_KEY_ID
    - AWS_SECRET_ACCESS_KEY
    - AWS_SESSION_TOKEN
    - CI_AWS_ECR_REGISTRY
    - CI_AWS_ECR_REPOSITORY_NAME
    - CLUSTER_NAME
    - SERVICE_NAME
    - TASK_DEFINITION_NAME

10. GitLab CI starten

